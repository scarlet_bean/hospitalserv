package com.hospitalsearch.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by speedfire on 4/28/16.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
